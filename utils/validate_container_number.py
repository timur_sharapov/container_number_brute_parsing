def validate_number(n):

    n = ''.join(item.upper() for item in n if item.isalnum())

    for index in range(0,4):
        if n[index] == "0":
            n = n.replace(n[index],"O")

    for index in range(4,11):
        if n[index] == "S":
            n = n.replace(n[index],"8")
    
    for index in range(4,11):
        if n[index] == "G":
            n = n.replace(n[index],"6")

    letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    numbers = [10, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 23,
               24, 25, 26, 27, 28, 29, 30, 31, 32, 34, 35, 36, 37, 38]
    d = dict(zip(list(letters), numbers))
    total = 0

    for index, char in enumerate(n[:-1]):
        if index < 4:
            total += d[char] * 2**index
        else:
            total += int(char) * 2**index

    check_total = total
    
    return n if (check_total - int(total/11) * 11) == int(n[-1]) else None