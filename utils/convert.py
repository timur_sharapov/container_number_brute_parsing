from wand.image import Image

def convert_to_jpeg(f):

    with(Image(filename=f, resolution=120)) as source: 
        images = source.sequence
        newfilename = f[:-4] + '.jpeg'
        Image(images[0]).save(filename=newfilename)
        
    return newfilename


# To support multiple pages
# pages = len(images)
# for i in range(pages):
            #newfilename = f[:-4] + '.jpeg'
            #Image(images[i]).save(filename=newfilename)