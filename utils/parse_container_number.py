import re


def get_container_numbers(txt):
    res = re.findall('[A-Za-z0]{3}[UJZujz]\.*\s*[0-9SG]{6}[-]?[0-9S]{1}', txt)
    return res
