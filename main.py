import argparse
import os

from utils.convert import convert_to_jpeg
from utils.OCR import get_text
from utils.parse_container_number import get_container_numbers
from utils.validate_container_number import validate_number

parser = argparse.ArgumentParser(description='Process input.')
parser.add_argument('--file_name', metavar='F',
                    type=str, help="Input a PDF file")
opt = parser.parse_args()


def parse_container_numbers():

    jpeg_file = convert_to_jpeg(opt.file_name)
    txt = get_text(jpeg_file)
    result = get_container_numbers(txt)
    os.remove(jpeg_file)
    result = [validate_number(item) for item in result]

    f = open("./output/" + jpeg_file[13:-4] + "txt", "w+")

    for item in result:
        if item is None:
            continue
        f.write(item + "\n")

if __name__ == "__main__":
    parse_container_numbers()
