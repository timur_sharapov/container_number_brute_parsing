# Container Number Brute Parser

This is the initial version of `Container Number Brute Parser`. The primary purpose of this small app is to extract all occurrences of container numbers in an input file. At this stage, the app has a command line interface as a proof of concept. 

## Input

A bill of lading in `pdf` format.For now, I assume that an input file contains only 1 page. Processing multiple pages can be easily configured in the future. 

## Output

A `.txt` file including the list of container numbers found in the input file. 

## How It Works

1. Read the input pdf file
2. Convert it to `jpeg`

The `Wand` library is used for conversion. In order to make it work, you also need to install `ImageMagick` and `Ghostscript`. 

3. Recognize all the text in the file

`Pytesseract` is used for recognition.

4. Parse container numbers (possible candidates)

Here, we use regular expressions to find all possible matches based on our knowledge of the container number format. 

5. Filter the candidates using checksum validation

Using the formula used for container number validation (according to the corresponding ISO standard), we can check whether the last digit is correct. 

6. Save the results (container numbers found) as `output/<filename>.txt`. 


## Requirements (versions used for testing)

* Python 3.7
* Pillow 6.2.0
* Wand 0.5.7
* Pytesseract 0.3.0
* Tesseract 5.0
* Ghostscript 9.27
* ImageMagick 6.9.10

Note that you need to install Tesseract in order to use Pytesseract and then, if you are using Windows, link pytesseract to it (see the code). If your Tesseract location is differnt, you need to make the appropriate changes in the code. 

## Usage Example

* Clone the repository
* Navigate to the source folder
* Using a terminal/command prompt, execute the following command: `py main.py --file_name cont.pdf`
* If you followed all the instructions above, you can now find the output `txt` file in the 'output' folder. 
* You can batch test by executing the following command: `py batch.py`


## Testing Stage

* You can find 50 BoL (Bill of Lading) samples extracted from Batch 5 in the `./test_files` folder. 
* To simplify testing, I created the `batch.py` to automatically test all the files inside the `./test_files` directory. The output is saved in `./output`. 

## Preliminary Results

You can view the preliminary testing results here:

[Results](http://tiny.cc/6wmwez)


## Issues and Possible Solutions

Several adjustments and tweaks have been made to improve parsing results. Nevertheless, the only source of inaccuracy seems to be related to the text output from Tesseract. 

* OCR errors due to low quality of input files

This can be improved by applying different types of filters to input images such as sharpening, resolution enhancement, deskewing etc. It is not easy to come up with a specific set of necessary improvements since input images are not uniform. Therefore, this issue needs further research and experiments. 

* OCR errors as a result of Tesseract's faulty recognition 

Even though I tried to fix some predictable recognition inaccuracies (e.g. `0` instead of `O`), some errors cannot and even must not be hardcoded. It does not make sense to train our own neural network for OCR since outperforming Tesseract is hardly possible and our use case is fairly standard. This type of issue can be solved exactly as the previous one -- using image preprocessing. 